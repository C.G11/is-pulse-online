import React, { useState, useEffect, useRef } from "react";
import {
    Container,
    Header,
    Label,
    Table,
    Progress,
    Loader
} from 'semantic-ui-react'
// SERVICES
import statusService from './services/statusService';

const style = {
    h1: {
        marginTop: '3em',
        color: '#fff'
    },
    h2: {
        margin: '4em 0em 2em',
        color: '#fff'

    },
    h3: {
        marginTop: '2em',
        padding: '2em 0em',
        color: '#fff'
    },
    footer: {
        marginBottom: 0,
        color: '#fff'

    }
}


function App() {
    const [info, setStatuses] = useState(null);
    const ref = useRef(null);

    useEffect(() => {
        ref.current = setInterval(getInfo, 1 * 60 * 1000);

        if (!info) {
            getInfo();
        }
        return () => {
            if(ref.current){
              clearInterval(ref.current)
            }
        }
    }, [])

    const getInfo = async () => {
        let res = await statusService.getStatus();
        setStatuses(res);
    }

    const renderInfo = info => {
        let success, error;
        if(info.status) {
            success = true;
            error = false;
        } else {
            success = false;
            error = true;
        }
        return (
            <Table.Row>
                <Table.Cell>
                    <Label color={success ? 'green': 'red'} horizontal>{success ? '100%': '0%'}</Label>
                    <a href={info.url} target="_blank" style={{'text-decoration': 'underline','letter-spacing': '3px' }}>{info.name}</a>
                </Table.Cell>
                <Table.Cell>
                    <Progress percent={100} success={success} error={error}>
                    </Progress>
                </Table.Cell>
            </Table.Row>
        );
    };

    return (
        <div>
            <Container>
                <Header as='h1' content='When Lunch, Sir ?' style={style.h2} textAlign='left' />
                <Container style={{color: '#fff', 'font-size': '19px'}}>
                    <p>
                        whenLunchSir.live is a website designed to track URLs and services related to Pulsechain, a much anticipated blockchain project from Richard Heart. Please note that whenLunchSir.live is not associated with Richard Heart or his development team.
                    </p>
                    <p>
                        We want to remind our users that the ONLY official URLs and RPC settings for Pulsechain are those shown on pulsechain.com, Gitlab, Pulsechain telegram group, and from directly from Richard Heart. Please be aware of fake websites and phishing scams that may try to deceive you.
                    </p>
                    <p>
                        This page tracks the uptime of Pulsechain-related URLs and services every 120 seconds, so you can be sure that the information we provide is as current as possible.
                    </p>
                </Container>
            </Container>
            <Container>
                <Header as='h3' textAlign='left' style={style.h3} content='Services' />
            </Container>

            <Container>
                <Table celled>
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell>URL</Table.HeaderCell>
                            <Table.HeaderCell>Status</Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>

                    <Table.Body>
                        {
                          (info && info.length > 0) ? (
                            info.map(i => renderInfo(i))
                            ) : (
                                <Loader active />
                            )
                        }
                    </Table.Body>
                </Table>
            </Container>
            <Container textAlign='center' style={style.footer}>
                <p>Donate: 0x052275572e9830D2b1BB4b3F9969512d0F44977b</p>

            </Container>
      
        </div>
    );
}

export default App;