import axios from 'axios';

export default {
  getStatus: async () => {
    let res = await axios.get(`/api/status`);
    return res.data || [];
  }
}